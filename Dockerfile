FROM ubuntu:20.04
LABEL maintainer="Matteo Meneghetti <matteo.meneghetti@univr.it>"

# Update packages and install dependencies
RUN apt update && \
    apt install -y python3-pip && \
    pip3 install -U platformio

RUN apt install -y build-essential
RUN apt install -y git

# Set the working directory
WORKDIR /workspace

# COPY ForecastNucleoFramework-test /app/forecast
# COPY build_and_upload.sh /app/forecast/
# RUN chmod +x /app/forecast/build_and_upload.sh
COPY framework-mbed@5.51105.220603 /root/.platformio/packages/framework-mbed@5.51105.220603
COPY tool-openocd@2.1000.200630 /root/.platformio/packages/tool-openocd@2.1000.200630
COPY toolchain-gccarmnoneeabi@1.70201.0 /root/.platformio/packages/toolchain-gccarmnoneeabi@1.70201.0
COPY tool-scons /root/.platformio/packages/tool-scons
COPY ststm32@5.3.0 /root/.platformio/platforms/ststm32@5.3.0

# Set the default command to PlatformIO
ENTRYPOINT ["pio"]
